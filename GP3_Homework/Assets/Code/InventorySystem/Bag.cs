﻿using UnityEngine;

namespace UEGP3.InventorySystem
{
    // TODO
    // below this is a unnecessary, empty line
    [CreateAssetMenu(fileName = "New Bag", menuName = "UEGP3/Inventory System/New Bag")]

    public class Bag : ScriptableObject
    {
        [Tooltip("The type of items that this bag can contain.")]
        [SerializeField]
        private Item _itemType;
        [Tooltip("The maximum count the bag can contain of this item.")]
        [SerializeField]
        private int _maximumCount;

        public Item ItemType => _itemType;
    }

}


