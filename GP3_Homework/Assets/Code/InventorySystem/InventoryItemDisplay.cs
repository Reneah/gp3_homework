﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace UEGP3.InventorySystem
{
    public class InventoryItemDisplay : MonoBehaviour
    {
        [Tooltip("The item that will be displayed")]
        [SerializeField]
        private Item item;
        [Tooltip("The text field for the item name")]
        [SerializeField]
        private TextMeshProUGUI _itemName;
        [Tooltip("The text field for the item description")]
        [SerializeField]
        private TextMeshProUGUI _itemDescription;
        [Tooltip("The text field for the item count")]
        [SerializeField]
        private TextMeshProUGUI _itemCount;
        [Tooltip("Reference to the player's inventory")]
        [SerializeField]
        private Inventory _playerInventory;

        private void Update()
        {
            // TODO
            // There is no need to set these each frame. Usually we would use a method like SetItem(Item item) that is then used to configure the ui values
            // in the moment the item for a slot changes. 
            _itemName.text = item.ItemName;
            _itemDescription.text = item.Description;
            
            // TODO same performance related problem as with the item pickup meshes
            GetComponentInChildren<Image>().sprite = item.ItemSprite;
        }

        public void UseItem()
        {
            _playerInventory.UseItem(item);    
        }
    }
}
