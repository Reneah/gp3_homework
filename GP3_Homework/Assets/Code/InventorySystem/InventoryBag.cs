﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UEGP3.InventorySystem
{
    public class InventoryBag : MonoBehaviour
    {
        [Tooltip("The scriptable object of the bag type.")]
        [SerializeField]
        private Bag _bagType;
        [SerializeField]
        Inventory _playerInventory;
        private ItemPickup[] _items;


        public void CheckItemType()
        {
            if(_bagType.ItemType)
            {
                Debug.Log($"Item accepted in {name}");
            }
        }
    }

}

