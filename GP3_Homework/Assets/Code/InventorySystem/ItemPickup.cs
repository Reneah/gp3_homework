﻿using System;
using UEGP3.Core;
using UnityEngine;

namespace UEGP3.InventorySystem
{
	[RequireComponent(typeof(SphereCollider))]
	public class ItemPickup : MonoBehaviour, ICollectible
	{
		[Tooltip("The scriptable object of the item to be picked up.")] [SerializeField]
		private Item _itemToPickup;
		[Tooltip("Range in which the pick up is performed.")] [SerializeField] 
		private float _pickupRange;
		[Tooltip("SFX that is being played when items are being picked up")] [SerializeField] 
		private ScriptableAudioEvent _pickupSFX;

		private AudioSource _audioSource;
		private SphereCollider _pickupCollider;
        private Mesh _itemMesh;

        // TODO as long as item pickups do not change dynamically, it would be best practice to only change the mesh once.
        // Calls like GetComponent (any variation) are quite performance heavy. If it can be avoided, we should do so.
        // In this specific case, we could do simply add the GetComponentInChildren call to the Awake method, cache the mesh
        // in a variable, e.g. _pickupMesh and then assign it to the mesh given in the itemToPickup SO. This way, we will only have 
        // to look for the component once, instead of each frame. 
        private void Update()
        {
	        // TODO 
	        // This is a similar issue as in the PlayerController-Update() loop, where you set the turnSmoothTime with a one-frame delay.
	        // if you press the Pause button before hitting play in Unity, you will notice that in the first frame, all meshes of items seem
	        // to be gone. This is because _itemMesh = null for the first frame. You assign the itemPickups mesh to _itemMesh (initially null)
	        // and just then you set the property of _itemMesh. This can easily be solved by changing the order of the two calls.
            // Get the mesh of the item's gaphics object
            GetComponentInChildren<MeshFilter>().mesh = _itemMesh;
            // Change mesh to the assigned mesh of the scriptable object item
            _itemMesh = _itemToPickup.ItemMesh;
        }

        private void Awake()
		{

			// Get collider on same object
			_pickupCollider = GetComponent<SphereCollider>();
			_audioSource = FindObjectOfType<AudioSource>();
			
			// Ensure collider values are set accordingly
			_pickupCollider.radius = _pickupRange;
			_pickupCollider.isTrigger = true;
		}

		public void Collect(Inventory inventory)
		{
			// Add item to inventory
			bool wasPickedUp = inventory.TryAddItem(_itemToPickup);

			// Destroy the pickup once the object has been successfully picked up
			if (wasPickedUp)
			{
				// Play the pickup SFX when picking up the item
				_pickupSFX.Play(_audioSource);
				Destroy(gameObject);
			}
		}
		
#if UNITY_EDITOR
		private void OnValidate()
		{
			if (!_pickupCollider)
			{
				_pickupCollider = GetComponent<SphereCollider>();
			}
			
			// Ensure radius is set correctly
			_pickupCollider.radius = _pickupRange;
		}
#endif
	}
}