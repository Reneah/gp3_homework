﻿using System;
using UnityEngine;

namespace UEGP3.PlayerSystem
{
	[RequireComponent(typeof(CharacterController))]
	public class PlayerController : MonoBehaviour
	{
		[Header("General Settings")] [Tooltip("The speed with which the player moves forward")] [SerializeField]
		private float _movementSpeed = 10f;
		[Tooltip("The graphical represenation of the character. It is used for things like rotation")] [SerializeField]
		private Transform _graphicsObject = null;
		[Tooltip("Reference to the game camera")] [SerializeField]
		private Transform _cameraTransform = null;

		[Header("Movement")] [Tooltip("Smoothing time for turns")] [SerializeField]
		private float _turnSmoothTime = 0.15f;
		[Tooltip("Smoothing time to reach target speed")] [SerializeField]
		private float _speedSmoothTime = 0.7f;
		[Tooltip("Modifier that manipulates the gravity set in Unitys Physics settings")] [SerializeField]
		private float _gravityModifier = 1.0f;
		[Tooltip("Maximum falling velocity the player can reach")] [Range(1f, 15f)] [SerializeField]
		private float _terminalVelocity = 10f;
		[Tooltip("The height in meters the cahracter can jump")] [SerializeField]
		private float _jumpHeight;
        [Tooltip("The power that the dash has.")]
        [SerializeField]
        private float _dashPower;
        [Tooltip("The amount the player can steer in the air")]
        [Range(0f, 1f)]
        [SerializeField]
        private float _airControlModifier = 1f;

        [Header("Ground Check")] [Tooltip("A transform used to detect the ground")] [SerializeField]
		private Transform _groundCheckTransform = null;
		[Tooltip("The radius around transform which is used to detect the ground")] [SerializeField]
		private float _groundCheckRadius = 0.1f;
		[Tooltip("A layermask used to exclude/include certain layers from the \"ground\"")] [SerializeField]
		private LayerMask _groundCheckLayerMask = default;

		// Use formula: Mathf.Sqrt(h * (-2) * g)
		private float JumpVelocity => Mathf.Sqrt(_jumpHeight * -2 * Physics.gravity.y);
        private float _dashVelocity => Mathf.Sqrt(_dashPower * -2 * Physics.gravity.y);

        private bool _isGrounded;
		private float _currentVerticalVelocity;
		private float _currentForwardVelocity;
		private float _speedSmoothVelocity;
		private CharacterController _characterController;
		private PlayerAnimationHandler _playerAnimationHandler;
        private float _airControl;
        private float _initialTurnSmoothTime;

        private void Awake()
		{
			_characterController = GetComponent<CharacterController>();
			_playerAnimationHandler = GetComponent<PlayerAnimationHandler>();
            _initialTurnSmoothTime = _turnSmoothTime;        
        }

		private void Update()
		{
			// Fetch inputs
			// GetAxisRaw : -1, +1 (0) 
			// GetAxis: [-1, +1]
			float horizontalInput = Input.GetAxisRaw("Horizontal");
			float verticalInput = Input.GetAxisRaw("Vertical");
			bool jumpDown = Input.GetButtonDown("Jump");
            // Fetch input for dashing
            bool dashForward = Input.GetButtonDown("Dash");
            // Calculate the air control
            _airControl = _initialTurnSmoothTime * _airControlModifier;

            // Calculate a direction from input data 
            Vector3 direction = new Vector3(horizontalInput, 0, verticalInput).normalized;
			
			// If the player has given any input, adjust the character rotation
			if (direction != Vector3.zero)
			{
				float lookRotationAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + _cameraTransform.eulerAngles.y;
				Quaternion targetRotation = Quaternion.Euler(0, lookRotationAngle, 0);
				// TODO check usage of this method GetSmoothTimeAfterAirControl
				_graphicsObject.rotation = Quaternion.Slerp(_graphicsObject.rotation, targetRotation, GetSmoothTimeAfterAirControl(_turnSmoothTime, false));
			}

			// Calculate velocity based on gravity formula: delta-y = 1/2 * g * t^2
			// We ignore the 1/2 to safe multiplications and because it feels better.
			// Second Time.deltaTime is done in controller.Move()-call so we save one multiplication here.
			_currentVerticalVelocity += Physics.gravity.y * _gravityModifier * Time.deltaTime;
			
			// Clamp velocity to reach no more than our defined terminal velocity
			_currentVerticalVelocity = Mathf.Clamp(_currentVerticalVelocity, -_terminalVelocity, JumpVelocity);

			// Calculate velocity vector based on gravity and speed
			// (0, 0, z) -> (0, y, z)
			float targetSpeed = (_movementSpeed * direction.magnitude);
			_currentForwardVelocity = Mathf.SmoothDamp(_currentForwardVelocity, targetSpeed, ref _speedSmoothVelocity, _speedSmoothTime);
			Vector3 velocity = _graphicsObject.forward * _currentForwardVelocity + Vector3.up * _currentVerticalVelocity;
			
			// Use the direction to move the character controller
			// direction.x * Time.deltaTime, direction.y * Time.deltaTime, ... -> resultingDirection.x * _movementSpeed
			// Time.deltaTime * _movementSpeed = res, res * direction.x, res * direction.y, ...
			_characterController.Move(velocity * Time.deltaTime);
			
			// Check if we are grounded, if so reset gravity
			_isGrounded = Physics.CheckSphere(_groundCheckTransform.position, _groundCheckRadius, _groundCheckLayerMask);
			// TODO by Kevin: This should be an if-else statement
			if (_isGrounded)
			{
				// Reset current vertical velocity
				_currentVerticalVelocity = 0f;
                _turnSmoothTime = _initialTurnSmoothTime;
            }
			// TODO this gets set at the end of the update loop, which means the updated turnSmoothTime will be used with one frame of delay
            // Check if we are not grounded, if so set air control
            if (!_isGrounded)
            {
                _turnSmoothTime = _airControl;
            }

			// If we are grounded and jump was pressed, jump
			if (_isGrounded && jumpDown)
			{
				_currentVerticalVelocity = JumpVelocity;
			}

            // If Dash was pressed, dash forward
            if(dashForward)
            {
                _currentForwardVelocity = _dashVelocity;
            }

			_playerAnimationHandler.SetMovementSpeed(_currentForwardVelocity);
		}
		
		// TODO check this method
		/// <summary>
		/// Calculates the smoothTime based on airControl.
		/// </summary>
		/// <param name="smoothTime">The initial smoothTIme</param>
		/// <param name="zeroControlIsMaxValue">If we do not have air control, is the smooth time float.MaxValue or float.MinValue?</param>
		/// <returns>The smoothTime after regarding air control</returns>
		private float GetSmoothTimeAfterAirControl(float smoothTime, bool zeroControlIsMaxValue)
		{
			// We are grounded, don't modify smoothTime
			if (_characterController.isGrounded)
			{
				return smoothTime;
			}

			// Avoid divide by 0 exception
			if (Math.Abs(_airControl) < Mathf.Epsilon)
			{
				// Different for different smoothing functions
				return zeroControlIsMaxValue ? float.MaxValue : float.MinValue;
			}

			// smoothTime is influenced by air control
			return smoothTime / _airControl;
		}
	}
}
